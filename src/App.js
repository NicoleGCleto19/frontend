import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {

  const base_url = 'http://localhost:3001'
  let users = axios.get(`${base_url}/users`).then( res => res  )

  console.log( users )

  return (
    <div className="App">
      <header className="App-header">
        <img 
          src="https://www.ingeniatec.org/public/static/img/logo-ingeniatec.svg" 
          className="App-logo" 
          alt="logo" 
        />
      </header>

      <div>

        { 
          users.map( item => {
            return item.username
          } )
        }

      </div>

    </div>
  );
}

export default App;
